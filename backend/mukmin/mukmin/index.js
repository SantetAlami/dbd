var path = require("path");
module.exports.main = async function (root, arg) {
  global["Mukmin"] = {};
  global["db"] = {};
  global.Mukmin["config"] = {};
  global.Mukmin["dataModel"] = {
    // 'id':'1',
    // 'username':'SantetAlami',
    // 'email':'jw029009@gmail.com',
    // 'complete_name':'JW',
    // 'verified':'1',
    // 'status':'1',
    // 'password':'$2y$10$9DwLj/IONqpDAoe9qE90CO20TNlaNuRfCo3w76qiTXU52UqOiT/YO',
    // 'gender':'male',
    // 'created':'2021-07-16 07:48:21',
    // 'updated':'2021-07-16 07:48:21',
  };
  global.Mukmin["dataStore"] = [];
  global.Mukmin["plugin_event"] = [];
  global.Mukmin["onWebBeforeLoad"] = [];
  global.Mukmin["plugin_spesification"] = {};
  global.Mukmin["root"] = root;
  global.Mukmin["blade"] = {};
  global.Mukmin["registerBlade"] = {};
  global.Mukmin["registerDataStore"] = {};
  global.Mukmin["arg"] = arg;
  global.Mukmin.getPath = relativePath => {
    return path.join(global.Mukmin.root, relativePath);
  };

  global.Mukmin.getDataStore = val => {
    try {
      if (val === undefined) {
        return global.Mukmin["dataStore"].default;
      } else {
        return global.Mukmin["dataStore"][val];
      }
    } catch (error) {
      throw new Error("data store not defined" + error.message);
    }
  };

  global.Mukmin.getDataModel = val => {
    try {
      if (val === undefined) {
        return global.Mukmin["dataModel"].default;
      } else {
        return global.Mukmin["dataModel"][val];
      }
    } catch (error) {
      throw new Error("Data store not defined" + error.message);
    }
  };

  global.Mukmin.registerDataStore = async (key, dataStore) => {
    global.Mukmin["dataStore"][key] = dataStore;
    global.Mukmin["dataModel"][key] = {};
  };
  global.Mukmin.registerDataModel = (keyDataStore, keyModel, model) => {
    global.Mukmin["dataModel"][keyDataStore][keyModel] = model;
  };

  global.Mukmin.addDataStore = val => {
    global.Mukmin["dataStore"].push(val);
  };
  global.Mukmin.registerBlade = (key, object) => {
    global.Mukmin["_" + key] = object;
  };
  global.Mukmin.registerPlugin = (key, object) => {
    global.Mukmin["__" + key] = object;
  };
  global.Mukmin.registerOnWebBeforeLoad = (extensionName, order, event) => {
    global.Mukmin["onWebBeforeLoad"].push({
      name: extensionName,
      order: order,
      event: event
    });
  };
  global.Mukmin.getAllOnWebBeforeLoad = () => {
    return global.Mukmin["onWebBeforeLoad"];
  };

  global.Mukmin.registerPluginWorker = (key, object) => {
    global.Mukmin["__" + key] = object;
  };

  global.Mukmin.registerConfig = (key, object) => {
    global.Mukmin["config"][key] = object;
  };

  global.Mukmin.getConfig = key => {
    try {
      return global.Mukmin["config"][key];
    } catch (error) {
      throw new Error("Config store not defined" + error.message);
    }
  };
  global.Mukmin.changeConfig = (key, variable, value) => {
    try {
      return global.Mukmin["config"][key][variable] = value;
    } catch (error) {
      throw new Error("Config store not defined" + error.message);
    }
  };
};
