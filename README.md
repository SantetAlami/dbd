## Instalation
```
# clone this repo
# cd frontend
# npm run build
# cd backend
# node app
```

## How to use admin CLI
Database designer user admin CLI to manage all backend. This can make simple  use this database designer in
private server without integration with email. 
`admin/admin.js` is main entry point.
###  Add user
```
# node admin/admin.js add user
```

### List user

```
# node admin/admin.js list user
```

### Edit user
To use edit username is required
```
# node admin/admin.js edit user [username]
```


### Get Detail user
To get detail, username is required
```
# node admin/admin.js get user [username]
```


### Get Hostname
```
# node admin/admin.js get hots_name 
```

### Set Hostname
```
# node admin/admin.js set hots_name [new name]
```

## Home view
![Database designer](docs/images/Home_database_designer.png)
## Open Project
![Database designer](docs/images/Open_project.png)
## Export to database
![Export to database](docs/images/Export_to_database.png)
## Share project
![Share](docs/images/Share_project.png)
